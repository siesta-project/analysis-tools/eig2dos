# ---
# Copyright (C) 1996-2016	The SIESTA group
#  This file is distributed under the terms of the
#  GNU General Public License: see COPYING in the top directory
#  or http://www.gnu.org/copyleft/gpl.txt .
# See Docs/Contributors.txt for a list of contributors.
# ---
.SUFFIXES:
.SUFFIXES: .f90 .F90 .o
#
all: Eig2DOS
#
OBJS=m_getopts.o
#
default: Eig2DOS
#
Eig2DOS: $(OBJS) Eig2DOS.o
	$(FC) -o $@ $(LDFLAGS) $(OBJS) Eig2DOS.o
#
clean:
	rm -f *.o *.mod Eig2DOS

# Dependencies
Eig2DOS.o: m_getopts.o
# Define default compilation methods
.F90.o:
	$(FC) -c $(FCFLAGS) $(INCFLAGS) $(FPPFLAGS) $< 
.f90.o:
	$(FC) -c $(FCFLAGS) $(INCFLAGS)  $<
